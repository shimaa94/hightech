
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ContactType(models.Model):
    _name ='contact.type'
    _rec_name = 'contact_type'
    contact_type =fields.Char("Contact Type")
    # contact_name = fields.Char("Contact Name")
    # phone_number = fields.Char("Phone Number")
    # status_id = fields.Many2one('status', string="Status")
    # type = fields.Char("Type")

class ContactTitle(models.Model):
      _name = 'contact.title'
      _rec_name = 'contact_title'
      contact_title = fields.Char("Contact Title")

class Category(models.Model):
      _name = 'category'
      _rec_name = 'category'
      category = fields.Char("Category")


class status(models.Model):
    _name = 'status'
    _rec_name = 'status'
    status = fields.Char("status")

class ContactName(models.Model):
    _name = 'contact.name'
    _rec_name = 'contact_name'
    contact_name = fields.Char("Contact Name")
    phone_number = fields.Char("Phone Number")
    status_id = fields.Many2one('status', string="Status")
    type_id = fields.Many2one('contact.type')
    res_id=fields.Many2one('res.partner')

class Governerate(models.Model):
    _name = 'governerate'
    _rec_name = 'governerate'
    res_govern_id = fields.Many2one('res.partner')
    contact_name_id = fields.Many2one('contact.name', string="Contact Name")
    governerate = fields.Char("governerate")
    status_id = fields.Many2one('status', string="Status")
    city_id = fields.Many2one('city',string="city")
    district_id = fields.Many2one('district',string="District")
    street = fields.Char(string="Street")
    building_number = fields.Char(string="Building")
    floor = fields.Char("Floor")
    department = fields.Char(string="Department")
    special_marque = fields.Char(string="Special Marque")

class City(models.Model):
    _name = 'city'
    _rec_name = 'city'
    city = fields.Char("City")

class District(models.Model):
    _name = 'district'
    _rec_name = 'district'
    district = fields.Char("District")

class CustomerWallet(models.Model):
    _name = 'customer.wallet'
    _rec_name = 'services_number'
    res_cust_id = fields.Many2one('res.partner')
    services_number = fields.Char("Services Number")
    services_Date = fields.Date("Services Date")
    payment = fields.Float(string="Payment")
    services_cost = fields.Float(string="Services Cost")
    balance = fields.Char(string="Balance")

class WarrantyDetails(models.Model):
    _name = 'warranty.details'
    _rec_name = 'product_family_indoor'
    res_warranty_id = fields.Many2one('res.partner')
    status_id = fields.Many2one('status', string="Status")
    product_family_indoor = fields.Char(string="Product Family")
    product_class_indoor = fields.Char("Product Class")
    product_capacity_indoor = fields.Char(string="Product Capacity")
    model_indoor = fields.Char(string="Model")
    indoor_serial_number = fields.Char(string="Indoor Serial Number")
    outdoor_serial_number = fields.Char(string="Outdoor Serial Number")
    buying_dealer = fields.Char(string="Buying Dealer")
    activation_date = fields.Date(string="Activation Date")
    installation_date = fields.Date(string="Installation Date")
    warranty_type = fields.Char(string="Warranty Type")
    activated = fields.Boolean(string="Activated?")
    contract_number = fields.Char(string="Contract Number")
class CustomerType(models.Model):
    _name ='customer.type'
    _rec_name = 'customer_type'
    customer_type =fields.Char("Customer Type")

class CustomerTitle(models.Model):
      _name = 'customer.title'
      _rec_name = 'customer_title'
      customer_title = fields.Char("Customer Title")


class Ownertype(models.Model):
    _name = 'owner.type'
    _rec_name = 'owner_type'
    owner_type = fields.Char("Owner Type")


class AssigneeType(models.Model):
    _name = 'assignee.type'
    _rec_name = 'assignee_type'
    assignee_type = fields.Char("assignee type")

class SrType(models.Model):
    _name = 'sr.type'
    _rec_name = 'sr_type'
    sr_type = fields.Char("Sr Type")


class SrStatus(models.Model):
    _name = 'sr.status'
    _rec_name = 'sr_status'
    sr_status = fields.Char("Sr Status")

class TaskTaskNew(models.Model):
    _name = 'task.task'
    _rec_name = 'task_number'
    res_task_id = fields.Many2one('res.partner')
    task_number = fields.Char('Task Number')
    task_name_id = fields.Many2one('project.task',string="Task Name")
    task_status_id = fields.Many2one('project.task.type',string="Task Status")
    task_priority = fields.Selection([('1', 'Low'), ('2', 'Normal'),],string="Task Priority")
    parent_task_id = fields.Many2one('project.task', string="Parent Task")
    duration = fields.Char('Duration')
    assignee = fields.Many2one('res.users', string="Assignee")
    schedual_date = fields.Date('Schedual Date')
    subject = fields.Char('Subject')

    follow_up_date = fields.Date('Follow Up Date')
    escalation_level = fields.Char('Escalation Level')
    complaint_reason = fields.Char('Complaint Reason')
    escalation_accuracy = fields.Char('Escalation Accuracy')
    channel_source = fields.Char('Source Of Channel')
    channel = fields.Char(string="channel")
