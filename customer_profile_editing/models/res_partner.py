from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit ='res.partner'
    image = fields.Binary('Image', attachment=True,
                          help="This field holds the image used as photo for the group, limited to 1024x1024px.")
    image_medium = fields.Binary('Medium', attachment=True,
                                 help="Medium-sized photo of the group. It is automatically " \
                                      "resized as a 128x128px image, with aspect ratio preserved. " \
                                      "Use this field in form views or some kanban views.")
    image_small = fields.Binary('Thumbnail', attachment=True,
                                help="Small-sized photo of the group. It is automatically " \
                                     "resized as a 64x64px image, with aspect ratio preserved. " \
                                     "Use this field anywhere a small image is required.")
    first_name = fields.Char("First Name")
    middle_name =fields.Char("middle Name")
    final_name = fields.Char("Final Name")
    full_name_En = fields.Char("Full Name (In English)")
    channel_username = fields.Char("Channel Username")
    facebook_url =fields.Char("Facebook URL")

    contact_type_id=fields.Many2one('contact.type',string="Contact Type")
    contact_title_id = fields.Many2one('contact.title', string="Contact Title")
    category_partner_id = fields.Many2one('category', string="Category")

    main_mobile_number = fields.Char("Main Mobile Number")
    mobile_number2 = fields.Char("Mobile Number2")
    mobile_number3 = fields.Char("Mobile Number3")
    fax = fields.Char("Fax")
    # email_partner = fields.Char("Email")

    governerate_code1 = fields.Char("Governerate Code1")
    lan_lind1 = fields.Char("Lan Lind1")
    governerate_code2 = fields.Char("Governerate Code2")
    lan_lind2 = fields.Char("Lan Lind2")

    contact_name_id = fields.Many2one('contact.name',string="Contact Name")
    contact_line_ids = fields.One2many('contact.name','res_id', string="Contact Name")
    phone_number = fields.Char("Phone Number")
    status_id = fields.Many2one('status',string="Status")
    type_id = fields.Many2one('contact.type',string="Contact Type")
    note = fields.Char("Note")

    governerate_id = fields.Many2one('governerate',string="Governerate")
    governerate_ids = fields.One2many('governerate','res_govern_id', string="Governerate")
    city_id = fields.Many2one('city',string="City")
    district_id = fields.Many2one('district',string="District")
    street_partner = fields.Char(string="Street")

    longitude = fields.Char(string="Longitude")
    latitude = fields.Char("Latitude")

    building_number = fields.Char(string="Building Number")
    floor = fields.Char("Floor")
    department = fields.Char(string="Department")
    special_marque = fields.Char(string="Special Marque")
    others = fields.Char(string="Other Info")

    customer_service_ids = fields.One2many('customer.wallet','res_cust_id')
    payment = fields.Float(string="Payment")
    services_cost = fields.Float(string="Services Cost")
    balance = fields.Char(string="Balance")

    warranty_details_ids = fields.One2many('warranty.details','res_warranty_id')
    product_family_indoor = fields.Char(string="Product Family")
    product_class_indoor = fields.Char("Product Class")
    product_capacity_indoor = fields.Char(string="Product Capacity")
    model_indoor = fields.Char(string="Model")
    description_indoor = fields.Char(string="Description")
    indoor_serial_number = fields.Char(string="Indoor Serial Number")
    outdoor_serial_number = fields.Char(string="Outdoor Serial Number")
    serial_dealer_name_indoor = fields.Char(string="Serial Dealer Name")
    issue_date_indoor = fields.Date(string="Issue Date")

    product_family_outdoor = fields.Char(string="Product Family")
    product_class_outdoor = fields.Char("Product Class")
    product_capacity_outdoor = fields.Char(string="Product Capacity")
    model_outdoor = fields.Char(string="Model")
    description_outdoor = fields.Char(string="Description")
    serial_dealer_name_outdoor = fields.Char(string="Serial Dealer Name")
    issue_date_outdoor = fields.Date(string="Issue Date")

    buying_dealer = fields.Char(string="Buying Dealer")
    buying_date = fields.Date(string="Buying Date")
    installation_date = fields.Date(string="Installation Date")
    warranty_type = fields.Char(string="Warranty Type")
    serial_number = fields.Char(string="Serial Number")

    sr_creation_date = fields.Date(string="Sr Creation Date")
    closed_date = fields.Date("Closed Date")
    sr_type_id = fields.Many2one('sr.type',string="Sr Type")
    sr_status_id = fields.Many2one('sr.status',string="Sr Status")
    severity = fields.Char(string="Severity")
    channel = fields.Char(string="channel")

    customer_type_id = fields.Many2one('customer.type',string="Customer Type")
    customer_name_id = fields.Many2one('res.partner',string="Customer Name")
    customer_title_id = fields.Many2one('customer.title',string="Customer Title")
    contact_customer_id = fields.Many2one('contact.name', string="Contact Name")
    contact_cust_title_id = fields.Many2one('contact.title', string="Contact Title")
    contact_relation = fields.Char(string="Contact Relation")

    task_ids = fields.One2many('task.task','res_task_id')
    task_number = fields.Char('Task Number')
    task_name_id = fields.Many2one('project.task',string="Task Name")
    task_status_id = fields.Many2one('project.task.type',string="Task Status")
    task_priority = fields.Selection([
        ('0', 'Low'), ('1', 'Medium'),
        ('2', 'High'), ('3', 'Highest')], required=True, default='1',string="Task Priority")
    parent_task_id = fields.Many2one('project.task', string="Parent Task")
    owner_type_id = fields.Many2one('owner.type', string="Owner Type")
    owner_id = fields.Many2one('res.users', string="Owner")
    duration = fields.Char('Duration')
    dealy_reason = fields.Char('Dealy Reason')
    assignee_type_id = fields.Many2one('assignee.type', string="Assignee Type")
    assignee = fields.Many2one('res.users', string="Assignee")
    schedual_date = fields.Date('Schedual Date')
    subject = fields.Char('Subject')
    descriptions = fields.Text('Descriptions')

    # task_ids2 = fields.One2many('task.task', 'res_task_id')
    follow_up_date = fields.Date('Follow Up Date')
    escalation_level = fields.Char('Escalation Level')

    complaint_reason = fields.Char('Complaint Reason')
    escalation_accuracy = fields.Char('Escalation Accuracy')
    channel_source = fields.Char('Source Of Channel')
    summary = fields.Text('Summary')






